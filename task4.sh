#!/bin/bash
useradd -m lucy
groupadd netadmin
usermod -a -G netadmin lucy
setfacl -m u:lucy:x /usr/sbin/tcpdump
